
public class Pessoa {

	private String nome;
	private int dataNascimento;
	private double altura;
		
	public Pessoa(String nome, int dataNascimento, double altura) {
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.altura = altura;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(int dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	public int calculaIdade(int dataAtual) {
		return dataAtual - dataNascimento;
	}
	
	public String mostrarDados() {
		return "Nome: " + nome + "\n" + "Data de Nascimento: " + dataNascimento + "\n" + "Altura: " + altura;
	}
	
}
